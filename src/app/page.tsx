'use client'
import AppTable from "@/components/app.table";
import Link from "next/link";
import React from "react";
import useSWR from "swr";

const fetcher = (url: string) => fetch(url).then((res) => res.json());

export default function Home() {
  
  const { data, error, isLoading } = useSWR(
    "http://localhost:8000/blogs",
    fetcher, {
    revalidateIfStale: false,
    revalidateOnFocus: false,
    revalidateOnReconnect: false
  });
  console.log("data >>>", data)
  if (!data) {
    return <div>Loading...</div>
  }
  return (
    <div>
      <div>
        <Link href="/youtube">Youtube Page</Link>
      </div>
      <div>
        <Link href="/facebook">Facebook Page</Link>
      </div>

      <div>
        <AppTable
          blogs={data}
        />
      </div>
    </div>
  );
}
