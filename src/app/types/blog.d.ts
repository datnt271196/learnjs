interface IBlog {
    author: string;
    content: string;
    id: number
    title: string;
}