'use client'
import {
    Table,
    TableBody,
    TableCell,
    TableHead,
    TableHeader,
    TableRow,
} from "@/components/ui/table"
import { Button } from "@/components/ui/button"
// import { useState } from "react";
import React from "react";
import CreateModal from "./create.modal";
interface IProps {
    blogs: IBlog[];
}

export default function AppTable(props: IProps) {
    const { blogs } = props;
    const [open, setOpen] = React.useState(false);
    console.log("props =>>>>>>>>>>", blogs);
    return (
        <>
        <Button onClick={() => setOpen(true)} variant="outline">Add New</Button>
        <Table>
            <TableHeader>
                <TableRow>
                    <TableHead>No</TableHead>
                    <TableHead>Title</TableHead>
                    <TableHead>Author</TableHead>
                    <TableHead>Actions</TableHead>
                </TableRow>
            </TableHeader>
            <TableBody>
                {blogs.map((blog) => (
                    <TableRow key={blog.id}>
                        <TableCell>{blog.id}</TableCell>
                        <TableCell>{blog.title}</TableCell>
                        <TableCell>{blog.author}</TableCell>
                        <TableCell className="text-right">
                            <Button className="mr-[4px]" variant="outline">Edit</Button>
                            <Button variant="outline">Delete</Button>
                        </TableCell>
                    </TableRow>
                ))}
            </TableBody>
        </Table>
        <CreateModal
        openModal={open}
        setOpenModal={() => setOpen(false)}
        />
        </>
        
        
    );
}